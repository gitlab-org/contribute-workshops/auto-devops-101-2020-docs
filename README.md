# Auto DevOps 101 - 2020 - docs

## Requirements

### Already setup for people attending the workshop

1. Group-level Kubernetes cluster (GKE), with base domain
1. Installed required k8s apps
1. Enabled Auto DevOps for group
 
### From you

1. Laptop
1. GitLab.com login
1. Developer access to https://gitlab.com/gitlab-org/contribute-workshops group. If you're member of gilab-org group you should already have it. Otherwise, please request access by following the link and clicking on "request access".

## Questions

Please prioritize using Slack channel #contribute-auto-devops-101-workshop for any questions. If you don't have access to this channel, feel free to use the Hopin chat.

## Let's do this

1. Go to https://gitlab.com/gitlab-org/contribute-workshops
1. Create a new project
1. Click on Import Project tab, Repo by URL :https://gitlab.com/contribute-auto-devops/minimal-ruby-app.git
1. Choose a unique name for your project, like your name. Set the project to public.
1. Create a Pipeline: Auto DevOps is enabled for the group, but CI/CD doesn’t run on project import so rather than pushing an arbitrary change, let’s just manually kick off the pipeline:
    1. Go to CI/CD >> Pipelines
    1. Click on `Run Pipeline`, submit `Run Pipeline`.

If you see your pipeline running, good job! You've completed your first task. ⭐️

Otherwise, [please reachout](#questions) if any problems happened along the way. We'll try to suport you.

Once the pipeline is complete, click on Operations -> Environments. You should see a `production` environment.
Click on the "Open live environment" icon to view the live production environment!

## Let's go further: Review Apps

1. Edit the server.rb file, change “Hello, World!” to “Hello, Earth!”
1. Choose a target branch name, like “earth”. Click “Commit changes”
1. On the New MR screen, click “Submit merge request”
1. A pipeline in the MR will be created which will also create the Review app automatically
1. Once the pipeline completes, click “View app” from the MR
1. You can also view the Security reports. Click “View full report” from the MR

If you got all the way here, Congrats! You've completed Auto DevOps 101. 🎉

Otherwise, don't give up! Let's try to find things out together. 💪